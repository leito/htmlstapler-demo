<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Prueba HtmlStapler - extension JSP</title>

        <!--
            Los CSS...
        -->
        <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet">
        <link href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
        <!--[if IE 7]>
        <link href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome-ie7.css" rel="stylesheet">
        <![endif]-->
        <link href="http://fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css">
        <link href="css/titulo.css" rel="stylesheet" type="text/css">
        <link href="css/subtexto.css" rel="stylesheet" type="text/css">

    </head>
    <body>
        <h1 id="titulo"></h1>
        <h3 id="subtexto"></h3>

        <!--
            Los JavaScript...
        -->
        <script src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
        <script src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>

        <script src="js/titulo.js"></script>
        <script src="js/subtexto.js"></script>

    </body>
</html>
